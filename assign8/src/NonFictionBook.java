
public abstract class NonFictionBook extends Publication
{

  public NonFictionBook()
  {
    super();
  }

  public NonFictionBook(String title, String author, int numOfPages,
      int yearOfPublication)
  {
    super(title, author, numOfPages, yearOfPublication);
  }
  
  public String toString()
  {
    return super.toString();
  }
}
