
public abstract class PeriodicPublication extends Publication
{
  private String schedule;

  public PeriodicPublication()
  {
    super();
    schedule = "";
  }

  public PeriodicPublication(String title, String author, int numOfPages,
      int yearOfPublication, String schedule)
  {
    super(title, author, numOfPages, yearOfPublication);
    this.schedule = schedule;
  }

  public String getSchedule()
  {
    return schedule;
  }

  public void setSchedule(String schedule)
  {
    this.schedule = schedule;
  }
  
  public String toString()
  {
    return super.toString() + "\nSchedule: " + schedule;
  }
}
