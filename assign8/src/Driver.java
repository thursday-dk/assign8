//********************************************************************
//File:	        Driver.java       
//Author:       Kalvin Dewey
//Date:	        11/25/17
//Course:       CPS100
//
//Problem Statement:
//Design and implement a set of classes that define various types
//of reading material: books, novels, magazines, technical jour-
//nals, textbooks, and so on. Include data values that describe
//various attributes of the material, such as the number of pages
//and the names of the primary characters. Include methods
//that are named appropriately for each class and that print an
//appropriate message. Create a main driver class to instantiate
//and exercise several of the classes.
//
//Inputs:	none 
//Outputs:	results of exercise
// 
//********************************************************************
public class Driver
{

  public static void main(String[] args)
  {
    Novel novel = new Novel("The Tragedy of the Box Ghost", "DannyPhan2013",
        5372, 2000 + 'x' + 'x', "Fanfiction", new String[]
        {"Danny Phantom", "Box Ghost"});//the year is 20xx joke
    System.out.println(novel);
    System.out.println();

    Magazine magazine = new Magazine();
    magazine.setSchedule("Weekly");
    System.out.println(magazine);
    System.out.println();
    
    Textbook textbook = new Textbook();
    textbook.setSubject("Reading Problem Statements");
    System.out.println(textbook);
    
  }

}
