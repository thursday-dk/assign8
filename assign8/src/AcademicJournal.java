
public class AcademicJournal extends PeriodicPublication
{

  public AcademicJournal()
  {
    super();
  }

  public AcademicJournal(String title, String author, int numOfPages,
      int yearOfPublication, String schedule)
  {
    super(title, author, numOfPages, yearOfPublication, schedule);
  }
  
  public String toString()
  {
    return super.toString();
  }
  
}
