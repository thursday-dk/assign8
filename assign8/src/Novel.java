
public class Novel extends FictionBook
{

  public Novel()
  {
    super();
  }

  public Novel(String title, String author, int numOfPages,
      int yearOfPublication, String genre, String[] mainCharacters)
  {
    super(title, author, numOfPages, yearOfPublication, genre, mainCharacters);
  }

  public String toString()
  {
    return super.toString();
  }

}
