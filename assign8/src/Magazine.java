
public class Magazine extends PeriodicPublication
{

  public Magazine()
  {
    super();
  }

  public Magazine(String title, String author, int numOfPages,
      int yearOfPublication, String schedule)
  {
    super(title, author, numOfPages, yearOfPublication, schedule);
  }
  
  public String toString()
  {
    return super.toString();
  }
  
}
