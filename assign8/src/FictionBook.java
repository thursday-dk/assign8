
public abstract class FictionBook extends Publication
{
  private String genre;
  private String[] mainCharacters;

  public FictionBook()
  {
    super();
    genre = "";
    mainCharacters = new String[0];
  }

  public FictionBook(String title, String author, int numOfPages,
      int yearOfPublication, String genre, String[] mainCharacters)
  {
    super(title, author, numOfPages, yearOfPublication);
    this.genre = genre;
    this.mainCharacters = mainCharacters;
  }

  public String getGenre()
  {
    return genre;
  }

  public void setGenre(String genre)
  {
    this.genre = genre;
  }

  public String[] getMainCharacters()
  {
    return mainCharacters;
  }

  public void setMainCharacters(String[] mainCharacters)
  {
    this.mainCharacters = mainCharacters;
  }
  
  public String toString()
  {
    String output = super.toString() + "\nGenre: " + genre;
    output += "\nMain characters: ";
    for (int i = 0; i < mainCharacters.length; i++)
    {
      output += '\n' + mainCharacters[i];
    }
    
    return output;
  }

}
