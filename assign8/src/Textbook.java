
public class Textbook extends NonFictionBook
{
  private String subject;

  public Textbook()
  {
    super();
    subject = "";
  }

  public Textbook(String title, String author, int numOfPages,
      int yearOfPublication, String subject)
  {
    super(title, author, numOfPages, yearOfPublication);
    this.subject = subject;
  }

  public String getSubject()
  {
    return subject;
  }

  public void setSubject(String subject)
  {
    this.subject = subject;
  }

  public String toString()
  {
    return super.toString() + "\nSubject: " + subject;
  }
}
