
public abstract class Publication
{
  private String title, author;
  private int numOfPages, yearOfPublication;

  public Publication()
  {
    title = "";
    author = "";
  }

  public Publication(String title, String author, int numOfPages,
      int yearOfPublication)
  {
    this.title = title;
    this.author = author;
    this.numOfPages = numOfPages;
    this.yearOfPublication = yearOfPublication;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getAuthor()
  {
    return author;
  }

  public void setAuthor(String author)
  {
    this.author = author;
  }

  public int getNumOfPages()
  {
    return numOfPages;
  }

  public void setNumOfPages(int numOfPages)
  {
    this.numOfPages = numOfPages;
  }

  public int getYearOfPublication()
  {
    return yearOfPublication;
  }

  public void setYearOfPublication(int yearOfPublication)
  {
    this.yearOfPublication = yearOfPublication;
  }

  public String toString()
  {
    return "Title: " + title + "\nAuthor: " + author + "\nNumber of pages: "
           + numOfPages + "\nYear of publication: " + yearOfPublication;
  }

}
